/**
 * MerchantExpressApiOperations.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package webreferences;

public interface MerchantExpressApiOperations extends java.rmi.Remote {
    public webreferences.TestResponse communicationTest(webreferences.TestRequest request) throws java.rmi.RemoteException;
    public webreferences.ShippedOrderResponse confirmShippedOrders(webreferences.ShippedOrderRequest request) throws java.rmi.RemoteException;
    public webreferences.ExpressTokenResponse createExpressToken(webreferences.ExpressTokenRequest request) throws java.rmi.RemoteException;
    public webreferences.RefundProcessResponse createRefund(webreferences.RefundProcessRequest request) throws java.rmi.RemoteException;
    public webreferences.OperationResponse getNewOperationActivity(webreferences.OperationActivityRequest request) throws java.rmi.RemoteException;
    public webreferences.OperationActivityNotifiedResponse confirmNewOperationActivity(webreferences.OperationActivityNotifiedRequest request) throws java.rmi.RemoteException;
    public webreferences.OperationResponse getOperation(webreferences.OperationRequest request) throws java.rmi.RemoteException;
}
